module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx}',
    './components/**/*.{js,ts,jsx,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        'app-red': '#EA4959',
        'app-blue': '#3A78F2',
        'app-cyan': '#9DE4DE',
        'app-white': '#FFFFFF',
        'app-black': '#232425',
        'app-gray': '#7C7C7C',
        'app-grayLight': '#C5C5C5',
        'app-light': '#F2F6FF',
      },
      backgroundImage: {
        'ari-per-country-bg':
          "url('../public/img/financing/ariPerCountryBg.png')",
      },
    },
    screens: {
      xs: '375px',
      sm: '600px',
      md: '900px',
      lg: '1200px',
      xl: '1536px',
    },
  },
  plugins: [],
};
