import Swal from 'sweetalert2';
import withReactContent from 'sweetalert2-react-content';

const appModal = withReactContent(
  Swal.mixin({
    showConfirmButton: false,
    customClass: {
      actions: 'app-modal-action',
      htmlContainer: 'app-html-container',
      popup: 'app-popup',
    },
  })
);

export { appModal };
