import cookie from 'js-cookie';
import { useEffect } from 'react';
import { appModal } from '../services/notifications.services';

function useModalClosingPage(comp: any) {
  useEffect(() => {
    //Select element
    const container = document.getElementById('container');
    //Execute event and function
    container?.addEventListener('mouseleave', activateModal);

    //create function
    function activateModal(e: any) {
      //only if mouse is out of top on screen
      if (e.clientY < 0 && !cookie.get('desactivateModal')) {
        //remove function and execute modal
        e.target.removeEventListener(e.type, activateModal);
        appModal.fire({
          html: comp,
          scrollbarPadding: false,
        });
        cookie.set('desactivateModal', 'true', { expires: 7 });
      }
    }
  }, []);
}

export default useModalClosingPage;
