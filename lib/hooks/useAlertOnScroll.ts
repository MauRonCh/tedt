import { useEffect, useState } from 'react';
import { SweetAlertOptions } from 'sweetalert2';
import { appModal } from '../services/notifications.services';

const useAlertOnScroll = (
  scroll: number,
  component: React.ReactElement,
  config?: SweetAlertOptions
) => {
  const [showAlert, setShowAlert] = useState(false);

  useEffect(() => {
    const handleScroll = () => {
      const currentScroll = window.pageYOffset;
      if (currentScroll >= scroll && !showAlert) {
        setShowAlert(true);
      }
    };

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, [scroll, showAlert]);

  useEffect(() => {
    if (showAlert) {
      appModal.fire({
        html: component,
        ...config,
      });
      // setShowAlert(false);
    }
  }, [showAlert, component, config]);

  return setShowAlert;
};

export default useAlertOnScroll;
