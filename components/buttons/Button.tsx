export interface IButton extends React.ComponentPropsWithoutRef<'button'> {
  children: React.ReactNode;
  color: 'white' | 'red';
  variant?: 'normal' | 'outlined';
  size?: 'md' | 'lg' | 'xl' | 'full';
  disabled?: boolean;
}
const Button: React.FC<IButton> = ({
  children,
  color,
  variant = 'normal',
  className,
  size = 'md',
  disabled = false,
  ...buttonProps
}) => {
  const styles = {
    normal: {
      white: 'text-app-red bg-app-white',
      red: 'text-app-white bg-app-red',
    },
    outlined: {
      white:
        'hover:bg-[rgba(255,255,255,0.3)] border-[2px] border-app-white text-app-white bg-transparent',
      red: 'border-[2px] border-app-red text-app-red bg-transparent',
    },
    size: {
      md: 'w-[175px] py-[10px] lg:w-[210px]',
      lg: 'min-w-[340px] py-[15px] lg:w-[420px]',
      xl: 'min-w-[232px] py-[15px] md:w-[294px]',
      full: 'w-full py-[15px]',
    },
  };

  return (
    <button
      className={`transition-all h-[50px] flex items-center justify-center rounded-[50px] ${
        styles?.size[size]
      } ${
        !disabled
          ? styles[variant][color] + ' hover:brightness-125 hover:scale-105'
          : 'text-app-white bg-[#c6cbd2] cursor-default'
      } ${className}`}
      {...buttonProps}
    >
      <span className="flex items-center justify-center relative gap-[15px]">
        {children}
      </span>
    </button>
  );
};
export default Button;
