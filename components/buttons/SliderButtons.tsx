import { useSwiper } from 'swiper/react';
import ButtonIcon from '../assets/ButtonIcon';

function SliderButtons() {
  const swiper = useSwiper();

  return (
    <div className="absolute top-0 right-0 hidden sm:flex gap-5 z-10 ">
      <button
        className="bg-[#EDEDED] w-9 h-9 font-semibold rounded-full flex justify-center items-center rotate-180"
        onClick={() => swiper.slidePrev()}
      >
        <ButtonIcon color="black" />
      </button>
      <button
        className="bg-[#EDEDED] w-9 h-9 font-semibold rounded-full flex justify-center items-center"
        onClick={() => swiper.slideNext()}
      >
        <ButtonIcon color="black" />
      </button>
    </div>
  );
}

export default SliderButtons;
