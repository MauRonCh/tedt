import { useScroll } from 'framer-motion';
import Link from 'next/link';
import React, { useEffect, useState } from 'react';
import Logo from '../assets/Logo';
import Toggle from '../assets/Toggle';
import Menu from './Menu';
import Tabs from './Tabs';

const NavBar: React.FC = () => {
  const [isOpen, setIsOpen] = useState(false);
  const { scrollY } = useScroll();
  const [scroll, setScroll] = useState(0);

  useEffect(() => {
    const changueScroll = (latest: number) => {
      setScroll(latest);
    };

    return scrollY.on('change', changueScroll);
  }, []);

  return (
    <>
      <nav
        id="navbar"
        className={`w-screen  fixed z-50 top-0 flex items-center justify-between px-7 md:justify-around transition-all duration-300  ${
          scroll > 0
            ? 'bg-white text-app-gray shadow-lg h-[70px]'
            : 'bg-transparent text-app-white h-[85px]'
        }`}
      >
        <Link href={'/'}>
          <Logo
            width={158}
            height={40}
            variant={scroll > 0 ? 'normal' : 'alternate'}
          />
        </Link>

        <Tabs scroll={scroll} />
        <Link
          href={'https://class-center.academlo.com'}
          className="hidden md:block"
          target="_blank"
        >
          <button
            className={`border rounded-full py-[5px] px-[22px] text-base leading-5 transition-all duration-300 ${
              scroll > 0
                ? 'border-app-red text-app-red'
                : 'border-app-white text-app-white'
            }`}
          >
            Plataforma
          </button>
        </Link>
        <button className="md:hidden" onClick={() => setIsOpen(!isOpen)}>
          <Toggle color={scroll > 0 ? 'black' : 'white'} />
        </button>
      </nav>
      <Menu isOpen={isOpen} setIsOpen={setIsOpen} />
    </>
  );
};

export default NavBar;
