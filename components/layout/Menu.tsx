import Image from 'next/image';
import Link from 'next/link';
import { Dispatch, SetStateAction } from 'react';
import Logo from '../assets/Logo';

interface IMenu {
  isOpen: boolean;
  setIsOpen: Dispatch<SetStateAction<boolean>>;
}

const Menu: React.FC<IMenu> = ({ isOpen, setIsOpen }) => {
  const tabs = [
    { name: 'Admisión', link: '/application' },
    { name: 'Financiamiento', link: '/financing' },
    { name: 'Estudiantes', link: '/students' },
    { name: 'Carreras', link: '/career' },
    { name: 'Plataforma', link: 'https://class-center.academlo.com' },
  ];

  return (
    <div
      className={`fixed w-screen h-[482px] bg-app-white left-0 z-50 ${
        isOpen ? 'top-0 opacity-100' : '-top-full opacity-0 pointer-events-none'
      } duration-500 ease-out shadow-[0px_3px_5px_rgba(0,0,0,0.2)]`}
    >
      <div className="h-[85px] w-screen border flex items-center justify-between px-7 shadow-[0px_3px_5px_rgba(0,0,0,0.2)]">
        <Link href={'/'} onClick={() => setIsOpen(false)}>
          <Logo width={130} height={40} />
        </Link>
        <button onClick={() => setIsOpen(false)}>
          <Image
            src={'/svg/closeButton.svg'}
            alt="close"
            width={22}
            height={22}
          />
        </button>
      </div>
      <nav className="flex flex-col gap-9 px-10 py-7 text-app-gray">
        {tabs.map((tab) => (
          <Link
            href={tab.link}
            className="border-b-2 border-transparent hover:border-app-white"
            key={tab.name}
            onClick={() => setIsOpen(false)}
          >
            {tab.name}
          </Link>
        ))}
      </nav>
    </div>
  );
};

export default Menu;
