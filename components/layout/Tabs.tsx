import Link from 'next/link';
import React from 'react';

interface ITabs {
  scroll: number;
}
const Tabs: React.FC<ITabs> = ({ scroll }) => {
  const tabs = [
    { name: 'Admisión', link: '/application' },
    { name: 'Financiamiento', link: '/financing' },
    { name: 'Estudiantes', link: '/students' },
    { name: 'Carrera', link: '/career' },
  ];

  return (
    <div className={` text-sm gap-10 hidden md:flex`}>
      {tabs.map((tab) => (
        <Link
          href={tab.link}
          className={`border-b-2 border-transparent ${
            scroll > 0 ? 'hover:border-app-gray' : 'hover:border-app-white'
          } `}
          key={tab.name}
        >
          {tab.name}
        </Link>
      ))}
    </div>
  );
};

export default Tabs;
