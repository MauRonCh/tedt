import useModalClosingPage from '@/lib/hooks/useModalClosingPage.hook';
import React from 'react';
import OutScreen from '../modal/OutScreen';
import Footer from './Footer';
import NavBar from './NavBar';

interface ILayout {
  children: React.ReactNode;
}

const Layout: React.FC<ILayout> = ({ children }) => {
  useModalClosingPage(<OutScreen />);
  return (
    <div className="overflow-x-hidden" id="container">
      <NavBar />
      <main className="app-container">{children}</main>
      <Footer />
    </div>
  );
};

export default Layout;
