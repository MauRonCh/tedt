import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';

const Footer: React.FC = () => {
  const footerContent = {
    countries: ['México', 'Estados Unidos', 'Argentina', 'España'],
    sections: [
      {
        name: 'Nosotros',
        elements: [
          {
            name: 'Profesores',
            link: '/',
          },
          {
            name: 'Cultura academlo',
            link: '/',
          },
          {
            name: 'ARI',
            link: '/financing',
          },
          {
            name: 'Súmate a nuestro equipo',
            link: '/',
          },
        ],
      },
      {
        name: 'Admisión',
        elements: [
          {
            name: 'Cursos',
            link: '/',
          },
          {
            name: 'Estudiantes',
            link: '/students',
          },
          {
            name: 'Proceso de admisión',
            link: '/',
          },
          {
            name: 'Preguntas frecuentes',
            link: '/faq',
          },
        ],
      },
      {
        name: 'Externos',
        elements: [
          {
            name: 'Noticias',
            link: 'https://www.youtube.com/@Academlo',
          },
          {
            name: 'Portafolios',
            link: '/',
          },
          {
            name: 'Bolsa de trabajo',
            link: '/',
          },
          {
            name: 'Aula virtual',
            link: 'https://class-center.academlo.com',
          },
        ],
      },
    ],
  };

  const router = useRouter();

  return (
    <footer
      className={`bg-app-black text-app-white p-4 lg:px-[8vw] md:pt-14 flex flex-col items-center ${
        router.pathname == '/404' || router.pathname == '/collaborators'
          ? 'hidden'
          : 'block'
      }`}
    >
      <div className="w-full max-w-[1440px] md:grid grid-cols-[1fr_3fr_2fr] grid-rows-[3fr_2fr] justify-items-center">
        <Image
          src={'/img/home/logo2.png'}
          width={164}
          height={62}
          alt="logo"
          className="block py-5 md:hidden"
        />
        {/* Country */}
        <ul className="text-xs md:text-sm flex flex-col gap-1 md:flex-col-reverse md:justify-self-start">
          {footerContent.countries.map((country) => (
            <li key={country}>{country}</li>
          ))}
        </ul>
        {/* Social */}
        <div className="flex gap-5 py-4 col-start-3 row-start-2 justify-self-end place-items-center md:pl-">
          <a href="https://www.facebook.com/academlo">
            <Image
              src={'/svg/social/facebook.svg'}
              width={0}
              height={0}
              alt="facebook"
              className="w-5 h-5 md:w-6 md:h-6"
            />
          </a>
          <a href="https://www.twitch.tv/academlo">
            <Image
              src={'/svg/social/twitch.svg'}
              alt="twitch"
              width={0}
              height={0}
              className="w-5 h-5 md:w-6 md:h-6"
            />
          </a>
          <a href="https://www.linkedin.com/school/academlo/">
            <Image
              src={'/svg/social/linkedin.svg'}
              alt="linkedin"
              width={0}
              height={0}
              className="w-5 h-5 md:w-6 md:h-6"
            />
          </a>
          <a href="https://github.com">
            <Image
              src={'/svg/social/github.svg'}
              alt="github"
              width={0}
              height={0}
              className="w-5 h-5 md:w-6 md:h-6"
            />
          </a>
        </div>
        {/* Sections */}
        <ul className="flex gap-4 md:gap-12 md:text-app-blue">
          {footerContent.sections.map((section) => (
            <li className="font-medium" key={section.name}>
              {section.name}
              <ul className="flex flex-col gap-2 py-5">
                {section.elements.map((element) => (
                  <li
                    className="text-md hover:text-app-white"
                    key={element.name}
                  >
                    <Link href={element.link}>{element.name}</Link>
                  </li>
                ))}
              </ul>
            </li>
          ))}
        </ul>
        {/* Form */}
        <div className="text-app-gray md:justify-self-end">
          <p className="text-md md:text-base font-medium w-64">
            Si quiere saber más de nosotros o tienes dudas puntuales
          </p>
          <p className="text-xs md:text-sm py-4">
            Déjanos tu mail y te contactaremos
          </p>
          <form className="py-4 flex flex-col md:flex-row justify-center items-center gap-6">
            <input
              type="text"
              className="outline-none border-2 border-app-white bg-transparent h-[50px] w-full px-3 text-app-white"
              placeholder="Escribe tu mail"
            />
            <button
              type="submit"
              className="bg-app-blue text-app-white px-8 py-2 w-28"
            >
              Enviar
            </button>
          </form>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
