import { LazyMotion, domAnimation, m } from 'framer-motion';

const PaintSection: React.FC = () => {
  return (
    <div className="app-banner">
      <div className="relative w-full h-[400px] bg-app-black overflow-hidden flex justify-center items-center">
        <LazyMotion features={domAnimation}>
          <m.div
            className="rounded-full bg-app-blue"
            whileInView={{
              translateY: [-200, 0, 0],
              width: [0, 20, 3000],
              height: [0, 20, 3000],
              backgroundColor: ['#E7CF00', '#003FE7'],
            }}
            transition={{
              duration: 2,
              ease: 'easeIn',
              times: [0, 0.5, 1],
            }}
          />
        </LazyMotion>
      </div>
    </div>
  );
};

export default PaintSection;
