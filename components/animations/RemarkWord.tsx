import { LazyMotion, domAnimation, m } from 'framer-motion';

interface IRemark {
  color?: string;
  word: string;
}

const RemarkWord: React.FC<IRemark> = ({ color = 'bg-app-blue', word }) => {
  return (
    <LazyMotion features={domAnimation}>
      <m.span
        className="relative px-3 h-fit"
        whileInView={{ color: '#FFFFFF' }}
        viewport={{ once: true }}
        transition={{
          duration: 1,
          ease: 'backInOut',
          delay: 0.3,
        }}
      >
        {word}
        <m.span
          className={`absolute left-0 top-0.5 -z-10 w-full h-full rounded-full ${color} text-app-white`}
          whileInView={{
            opacity: [0, 1],
            scaleX: [0, 1],
            originX: 0,
          }}
          viewport={{ once: true }}
          transition={{
            duration: 1,
            ease: 'backInOut',
            delay: 0.3,
          }}
        />
      </m.span>
    </LazyMotion>
  );
};

export default RemarkWord;
