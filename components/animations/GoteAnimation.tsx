import { LazyMotion, domAnimation, m, useScroll } from 'framer-motion';
import { useEffect, useState } from 'react';

const GoteAnimation = () => {
  const { scrollY } = useScroll();
  const [scroll, setScroll] = useState(0);

  useEffect(() => {
    const changueScroll = (latest: number) => {
      setScroll(latest);
    };

    return scrollY.on('change', changueScroll);
  }, []);

  console.log(scroll);

  return (
    <LazyMotion features={domAnimation}>
      <m.div
        initial={scroll == 200 ? { visibility: 'hidden' } : ''}
        animate={{ top: scroll }}
        className={`fixed bg-black rounded-full w-5 h-5 z-50`}
      />
    </LazyMotion>
  );
};

export default GoteAnimation;
