import { LazyMotion, domAnimation, m } from 'framer-motion';
import React, { useEffect, useState } from 'react';

interface ITyping {
  words?: String[];
}

const TypingAnimation: React.FC<ITyping> = ({ words }) => {
  const [count, setCount] = useState(0);
  const [word, setWord]: any = useState(words?.[count]);

  useEffect(() => {
    if (words) {
      setTimeout(() => {
        setCount(count + 1);
        setWord(words[(count + 1) % words.length]);
      }, 5000);
    }
  }, [count]);

  return (
    <LazyMotion features={domAnimation}>
      <m.span className="relative whitespace-nowrap">
        {word}
        <m.span
          animate={{ width: ['100%', '0%', '100%'] }}
          transition={{ duration: 5, repeat: Infinity }}
          className="h-full bg-app-red absolute right-0 top-0"
        >
          <m.span
            animate={{ height: ['100%', '0%', '100%'] }}
            transition={{ duration: 1, repeat: Infinity, type: 'tween' }}
            className="w-1 h-full bg-white absolute left-0 top-[50%] translate-y-[-50%]"
          />
        </m.span>
      </m.span>
    </LazyMotion>
  );
};

export default TypingAnimation;
