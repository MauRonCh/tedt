import Image from 'next/image';
import { Autoplay } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import { Swiper, SwiperSlide } from 'swiper/react';
import brands from '../../lib/data/sliders/brands.json';

export interface IBrandSlider {
  reverse?: boolean;
}

export interface IBrand {
  id: number;
  img: string;
}

const BrandsSlider = ({ reverse = false }: IBrandSlider) => {
  const dirArray = [...brands];

  if (reverse) {
    dirArray.reverse();
  }

  return (
    <Swiper
      className="brandSlider"
      slidesPerView={'auto'}
      loop={true}
      modules={[Autoplay]}
      speed={5000}
      autoplay={{
        delay: 0,
        disableOnInteraction: true,
        stopOnLastSlide: false,
        reverseDirection: reverse,
        pauseOnMouseEnter: true,
      }}
      allowTouchMove={false}
      breakpoints={{
        300: {
          slidesPerView: 3,
          spaceBetween: 30,
        },
        600: {
          slidesPerView: 3.3,
          spaceBetween: 30,
        },
        800: {
          slidesPerView: 3.5,
          spaceBetween: 50,
        },
        1000: {
          slidesPerView: 5,
          spaceBetween: 50,
        },
        1300: {
          slidesPerView: 7,
          spaceBetween: 50,
        },
        1400: {
          slidesPerView: 9,
          spaceBetween: 50,
        },
      }}
    >
      {dirArray.map((brand: IBrand) => (
        <SwiperSlide key={brand.id}>
          <div className="grayscale duration-500 hover:grayscale-0 hover:scale-75">
            <Image
              loading="lazy"
              src={brand.img}
              width={300}
              height={50}
              alt={''}
            />
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default BrandsSlider;
