import 'swiper/css';
import { Swiper, SwiperSlide } from 'swiper/react';
import info from '../../lib/data/sliders/academloPlus.json';
const AcademloPlusSlider = () => {
  return (
    <Swiper
      grabCursor
      slidesPerView={1.5}
      className="app-container md:col-span-2"
      spaceBetween={20}
      breakpoints={{
        600: {
          slidesPerView: 2,
        },
        900: {
          slidesPerView: 3,
        },
        1400: {
          slidesPerView: 3.5,
        },
        1700: {
          slidesPerView: 4.5,
        },
      }}
    >
      {info?.map((x) => (
        <SwiperSlide key={x.id} className="border-r-2 border-app-blue">
          <div className="max-w-xs sm:px-4 h-[180px] sm:h-[220px]">
            <h4 className="app-title-4 font-bold text-app-blue mb-6">
              {x.title}
            </h4>
            <p className="app-title-6">{x.description}</p>
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default AcademloPlusSlider;
