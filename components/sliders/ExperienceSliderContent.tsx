import Image from 'next/image';
import { useSwiperSlide } from 'swiper/react';

interface IExperience {
  title: string;
  description: string;
  description2?: string;
  img: string;
}

const ExperienceSliderContent: React.FC<IExperience> = ({
  title,
  description,
  description2,
  img,
}) => {
  const swiperSlide = useSwiperSlide();
  return (
    <>
      <div className="w-64 h-2 bg-app-grayLight absolute rounded-md">
        <div
          className={`h-2 bg-app-blue absolute rounded-md ${
            swiperSlide.isVisible ? 'w-full duration-[15000ms]' : 'w-0'
          } transition-[width] ease-linear`}
        ></div>
      </div>

      <div
        className={`cursor-grab ${
          swiperSlide.isVisible ? 'opacity-100' : 'opacity-0'
        } duration-700 ease-in-out mx-auto relative grid grid-cols-1 grid-rows-2 sm:grid-cols-2 sm:grid-rows-1 gap-4 sm:h-[520px] md:h-[320px] h-[650px] pt-10`}
      >
        <div className="pl-12 sm:p-0 row-start-2 sm:row-start-1 mr-4">
          <h3 className="flex items-start app-title-4 text-app-blue gap-2 relative mb-2">
            <Image
              src="/svg/people.svg"
              width={0}
              height={0}
              alt="people"
              className="w-6 h-6 sm:w-11 sm:h-11 absolute sm:relative -left-8 sm:left-0"
            />
            {title}
          </h3>
          <div className="flex flex-col gap-4">
            <p className="app-title-6 text-app-gray">{description}</p>
            {description2 && (
              <p className="app-title-6 text-app-gray">{description2}</p>
            )}
          </div>
        </div>
        <img
          src={img}
          alt="peopleImg"
          className="h-60 m-auto px-5 md:h-full object-contain"
        />
      </div>
    </>
  );
};

export default ExperienceSliderContent;
