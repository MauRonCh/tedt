import LiteYouTubeEmbed from 'react-lite-youtube-embed';
import { EffectCoverflow } from 'swiper';
import 'swiper/css';
import 'swiper/css/effect-coverflow';
import { Swiper, SwiperSlide } from 'swiper/react';
import stories from '../../lib/data/sliders/stories.json';

export interface IStoriesSlider {
  viewAll?: boolean;
}

const StoriesSlider = ({ viewAll = true }: IStoriesSlider) => {
  return (
    <Swiper
      // width={1000}
      watchOverflow={true}
      centeredSlides={true}
      slidesPerView={1.5}
      effect={'coverflow'}
      coverflowEffect={{
        rotate: 0,
        stretch: 0,
        depth: 400,
        modifier: 1,
        slideShadows: false,
        scale: 0.8,
      }}
      modules={[EffectCoverflow]}
      className="min-w-[345px] h-[50vw] max-h-[545px]"
      grabCursor
      breakpoints={{
        '300': {
          slidesPerView: 1,
        },
        '600': {
          slidesPerView: 1.5,
        },
        '2500': {
          slidesPerView: 3.5,
        },
      }}
    >
      {viewAll
        ? stories?.map((story) => (
            <SwiperSlide key={story.id}>
              {({ isActive }) => (
                <div
                  className={`w-full h-full relative m-auto px-8 md:px-20 sm:max-w-[1100px] overflow-visible`}
                >
                  {/* <iframe
                    className="w-full h-full rounded-lg sm:rounded-[40px]"
                    width="100%"
                    height="100%"
                    src={`${story?.link}?autoplay=0&loop=1&modestbranding=1&rel=0`}
                    allowFullScreen={true}
                    title="Academlo ARI"
                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  ></iframe> */}
                  <span
                    className={`w-full h-full bg-[#3a78f240] absolute top-0 left-0 ${
                      isActive && 'hidden'
                    }`}
                  ></span>
                </div>
              )}
            </SwiperSlide>
          ))
        : stories
            ?.map((story) => (
              <SwiperSlide key={story.id}>
                {({ isActive }) => (
                  <div
                    className={`w-full h-full relative m-auto px-8 md:px-20 sm:max-w-[1100px] overflow-visible`}
                  >
                    <LiteYouTubeEmbed id={story.link} title={story.title} />
                    {/* <iframe
                      className="w-full h-full rounded-lg sm:rounded-[40px]"
                      width="100%"
                      height="100%"
                      src={`${story?.link}?autoplay=0&loop=1&modestbranding=1&rel=0`}
                      allowFullScreen={true}
                      title="Academlo ARI"
                      allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                    ></iframe> */}
                    <span
                      className={`w-full h-full bg-[#3a78f240] absolute top-0 left-0 ${
                        isActive && 'hidden'
                      }`}
                    ></span>
                  </div>
                )}
              </SwiperSlide>
            ))
            .slice(0, 6)}
    </Swiper>
  );
};

export default StoriesSlider;
