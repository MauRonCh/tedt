import 'swiper/css';
import { Swiper, SwiperSlide } from 'swiper/react';
import services from '../../lib/data/sliders/services.json';
import ServicesCard from '../cards/ServicesCard';
const ServicesSlider = () => {
  return (
    <Swiper
      grabCursor
      watchSlidesProgress={true}
      className="w-full h-auto"
      spaceBetween={50}
      breakpoints={{
        375: {
          slidesPerView: 1.5,
        },
        440: {
          slidesPerView: 1.8,
        },
        740: {
          slidesPerView: 2.3,
        },
        875: {
          slidesPerView: 2.6,
        },
        1100: {
          slidesPerView: 2.8,
        },
        1400: {
          slidesPerView: 3.5,
        },
      }}
    >
      {services?.map((service) => (
        <SwiperSlide key={service.id}>
          <ServicesCard
            title={service.title}
            description={service.description}
            link={service.link}
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default ServicesSlider;
