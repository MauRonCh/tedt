import React from 'react';
import { Autoplay, EffectFade } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import experienceData from '../../lib/data/sliders/experienceData.json';
import SliderButtons from '../buttons/SliderButtons';
import ExperienceSliderContent from './ExperienceSliderContent';

const ExperienceSlider: React.FC = () => {
  return (
    <Swiper
      className="relative"
      loop={true}
      loopPreventsSliding={false}
      slidesPerView={1}
      effect="fade"
      autoplay={{
        delay: 15000,
        waitForTransition: false,
      }}
      modules={[EffectFade, Autoplay]}
    >
      <SliderButtons />
      {experienceData.map((data) => (
        <SwiperSlide key={data.title}>
          <ExperienceSliderContent
            key={data.title}
            title={data.title}
            description={data.description}
            description2={data.description2}
            img={data.img}
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default ExperienceSlider;
