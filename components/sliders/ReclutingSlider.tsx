import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/pagination';
import { Pagination } from 'swiper';
import reclutingData from '../../lib/data/sliders/recluting.json';

const ReclutingSlider = () => {
  return (
    <Swiper
      pagination={true}
      modules={[Pagination]}
      className="reclutingSlider"
      grabCursor
      spaceBetween={50}
      loop
    >
      {reclutingData?.map((reclutingElement) => (
        <SwiperSlide className="pb-20" key={reclutingElement.id}>
          <div className="w-full min-h-[450px] flex flex-col border-[1px] border-app-grayLight rounded-xl px-7 py-14 md:flex-row md:min-h-[350px]">
            <div className="md:mr-6">
              <p className="app-title-4 text-app-blue mb-2 w-[80%] sm:mb-5 sm:w-full">
                {reclutingElement.title}
              </p>
              <p className="app-title-6 text-app-gray mb-8 md:w-[60%]">
                {reclutingElement.description}
              </p>
            </div>
            <div
              style={{ backgroundImage: `url(${reclutingElement.img})` }}
              className="w-full min-w-[290px] h-[48vw] max-w-[425px] max-h-[265px] mx-auto bg-center bg-cover bg-no-repeat rounded-xl"
            ></div>
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default ReclutingSlider;
