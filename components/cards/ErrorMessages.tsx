import { useEffect, useState } from 'react';

const ErrorMessages: React.FC = () => {
  const [num, setNum] = useState(0);

  const errs = [
    {
      selector: '#titanic',
      code: {
        prop: 'float: ',
        value: 'none;',
      },
      important: false,
    },
    {
      selector: '.gobierno',
      code: {
        prop: 'transition: ',
        value: 'all 4yr ease.-out;',
      },
      important: false,
    },
    {
      selector: '#EEUU + Mexico',
      code: {
        prop: 'border: ',
        value: '1px dashed;',
      },
      important: false,
    },
    {
      selector: '#big-bang::before',
      code: {
        prop: 'content: ',
        value: '"";',
      },
      important: false,
    },
    {
      selector: '.KKK',
      code: {
        prop: 'color: ',
        value: 'white',
      },
      important: true,
    },
    {
      selector: '.esposa',
      code: {
        prop: 'right: ',
        value: '100%;',
      },
      code2: {
        prop: 'margin: ',
        value: '0%;',
      },
      important: false,
    },
  ];
  useEffect(() => {
    setNum(Math.floor(Math.random() * 6));
  }, []);

  return (
    <div className="flex flex-col gap-6 pl-16 pt-5">
      <p className="text-app-cyan">{errs[num].selector} &#123;</p>
      <div className="text-app-red pl-10 flex gap-4">
        <p>
          {errs[num].code?.prop}
          <span className="text-app-white">{errs[num].code?.value}</span>
        </p>
        <p className={`${!errs[num].code2 && 'hidden'}`}>
          {errs[num].code2?.prop}
          <span className="text-app-white">{errs[num].code2?.value}</span>
        </p>
        <span className="text-yellow-300">
          {errs[num].important && '!important;'}
        </span>
      </div>
      <p className="text-app-cyan">&#125;</p>
    </div>
  );
};

export default ErrorMessages;
