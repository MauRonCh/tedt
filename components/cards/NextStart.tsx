import Link from 'next/link';
import data from '../../lib/data/start-dates/nextStart-Home.json';
import ButtonIcon from '../assets/ButtonIcon';
import Button from '../buttons/Button';

const NextStart: React.FC = () => {
  return (
    <div className="max-w-[842px] mx-auto border-2 border-app-red rounded-3xl flex flex-col items-center text-center">
      <h3 className="pt-14 md:pt-9 pb-4 text-xl sm:text-2xl font-normal">
        Próximo inicio:
      </h3>
      <h2 className="text-xl sm:text-4xl font-semibold px-10 sm:px-28 md:px-40">
        {data.title}
      </h2>
      <p className="app-title-4 font-normal text-app-red pt-7 sm:pt-9 leading-7">
        {data.vacantsText}
      </p>
      <p className="app-title-2 font-semibold leading-10">
        {'Inicia ' + data.date}
      </p>
      <ul className="flex flex-wrap justify-center gap-2 sm:px-0 text-xs sm:text-base font-medium mb-16 mt-11">
        {data.infoCourse.map((info) => (
          <li
            className="bg-transparent border-app-gray border text-app-gray min-w-[136px] sm:w-fit sm:px-5 rounded-[20px] py-2"
            key={info}
          >
            {info}
          </li>
        ))}
      </ul>
      <Link href="/career#modules">
        <Button color="red" size="xl" className="relative -bottom-6">
          Ver malla curricular
          <ButtonIcon color="white" />
        </Button>
      </Link>
    </div>
  );
};

export default NextStart;
