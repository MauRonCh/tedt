import React from 'react';
import ButtonIcon from '../assets/ButtonIcon';
import Button from '../buttons/Button';

export interface ServiceCard {
  title?: string;
  description?: string;
  link?: string;
}

const ServicesCard: React.FC<ServiceCard> = ({ title, description }) => {
  return (
    <article className="relative min-w-[185px] h-full text-app-white sm:min-w-[275px]">
      <div className="absolute right-0 w-auto h-[200px] border-r-[2px] border-r-white sm:h-[400px]"></div>
      <header className="w-[70%] app-title-4">{title}</header>
      <p className="w-[90%] text-[#DCDCDC] text-[12px] app-font-WorkSans mt-[15px] sm:ml-[5px]  sm:text-[24px] mb-[35px]">
        {description}
      </p>
      <Button color="white">
        <p>Me interesa</p>
        <ButtonIcon color="red" />
      </Button>
    </article>
  );
};

export default ServicesCard;
