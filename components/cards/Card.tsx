import { LazyMotion, domAnimation, m } from 'framer-motion';
import Image from 'next/image';
import React from 'react';

interface ICard {
  title: string;
  description: string;
  img: string;
  color: string;
}
const Card: React.FC<ICard> = ({ title, description, img, color }) => {
  const rotating = {
    rest: { marginRight: '0px', rotate: 0 },
    hover: {
      marginRight: '20px',
      rotate: 3,
    },
  };

  return (
    <LazyMotion features={domAnimation}>
      <m.article
        initial="rest"
        whileHover="hover"
        animate="rest"
        className="relative"
      >
        <m.div
          variants={rotating}
          transition={{ duration: 0.2 }}
          className="h-full origin-bottom-left border-2 p-3 border-[#DCDCDC] flex md:flex-col md:p-12 md:w-[280px] lg:w-[320px] gap-4 items-start rounded-2xl"
        >
          <div className="relative w-[56px] md:w-[100px] h-[56px] md:h-[100px] flex justify-center items-center">
            <svg
              width="88"
              height="98"
              viewBox="0 0 88 98"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M38.5 2.17543C41.9034 0.210466 46.0966 0.210466 49.5 2.17543L81.8013 20.8246C85.2047 22.7895 87.3013 26.4209 87.3013 30.3509V67.6491C87.3013 71.5791 85.2047 75.2105 81.8013 77.1754L49.5 95.8246C46.0966 97.7895 41.9034 97.7895 38.5 95.8246L6.19873 77.1754C2.79532 75.2105 0.69873 71.5791 0.69873 67.6491V30.3509C0.69873 26.4209 2.79532 22.7895 6.19873 20.8246L38.5 2.17543Z"
                fill={color === 'red' ? '#EA4959' : '#3A78F2'}
              />
            </svg>

            <Image src={img} alt={'icon'} fill={true} />
          </div>
          <div className="flex flex-col gap-2">
            <h3 className={`app-title-4 text-app-${color}`}>{title}</h3>
            <p
              className={`app-title-6 ${
                color == 'red' ? 'text-app-white' : 'text-app-gray'
              } pb-4`}
            >
              {description}
            </p>
          </div>
        </m.div>
      </m.article>
    </LazyMotion>
  );
};

export default Card;
