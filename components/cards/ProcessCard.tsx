import processInfo from '../../lib/data/processInfo.json';
import Card from './Card';

interface IProcessCard {
  alternate: boolean;
}

const ProcessCard: React.FC<IProcessCard> = ({ alternate }) => {
  return (
    <div className="flex flex-col md:mx-auto max-w-[1000px]">
      <div className=" flex flex-col md:flex-row gap-4 mb-12">
        {processInfo.map((process) => (
          <Card
            key={process.id}
            img={process.image}
            title={process.title}
            description={process.description}
            color={alternate ? 'red' : 'blue'}
          />
        ))}
      </div>
    </div>
  );
};

export default ProcessCard;
