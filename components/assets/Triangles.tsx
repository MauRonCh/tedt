interface ITriangles extends React.ComponentPropsWithoutRef<'svg'> {
  colorOutline?: string;
  colorFill?: string;
}

const Triangles: React.FC<ITriangles> = ({
  colorOutline = '#9DE4DE',
  colorFill = '#9DE4DE',
  ...svgProps
}) => {
  return (
    <svg
      width="151"
      height="148"
      viewBox="0 0 151 148"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...svgProps}
    >
      <path
        d="M127.351 9.78547C134.055 9.96732 139.356 15.5251 139.22 22.23L137.744 95.126C137.524 106.015 124.231 111.182 116.715 103.3L45.3062 28.4275C37.7896 20.5461 43.58 7.51317 54.4671 7.80848L127.351 9.78547Z"
        stroke={colorOutline}
        strokeWidth="1"
      />
      <path
        d="M106.583 19.5453C115.152 19.5566 122.03 26.6226 121.811 35.1887L119.57 122.57C119.224 136.065 102.809 142.491 93.3931 132.818L8.22324 45.3212C-1.19272 35.6479 5.67359 19.412 19.1729 19.4298L106.583 19.5453Z"
        fill={colorFill}
      />
    </svg>
  );
};

export default Triangles;
