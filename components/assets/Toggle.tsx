interface IToggle extends React.ComponentPropsWithoutRef<'svg'> {
  color?: string;
}

const Toggle: React.FC<IToggle> = ({ color = 'black', ...svgProps }) => {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...svgProps}
    >
      <path d="M3 7H21" stroke={color} strokeWidth="2" strokeLinecap="round" />
      <path
        d="M9.49023 12H21.0002"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path
        d="M3 12H5.99"
        stroke={color}
        strokeWidth="2"
        strokeLinecap="round"
      />
      <path d="M3 17H21" stroke={color} strokeWidth="2" strokeLinecap="round" />
    </svg>
  );
};

export default Toggle;
