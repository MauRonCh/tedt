import Image from 'next/image';
import Link from 'next/link';
import { IoMdClose } from 'react-icons/io';
import Swal from 'sweetalert2';
import Button from '../buttons/Button';

export default function OutScreen() {
  const countries = [
    {
      name: 'México',
      ytlink: 'https://youtu.be/MErb6oNkXzU',
    },
    {
      name: 'Colombia',
      ytlink: 'https://youtu.be/yQo0Re6A8iY',
    },
    {
      name: 'Chile',
      ytlink: 'https://youtu.be/uID-LG3Vf7Q',
    },
    {
      name: 'Perú',
      ytlink: 'https://youtu.be/3HlGtHqmIpA',
    },
  ];

  return (
    <aside className="grid grid-cols-[2fr_2.3fr] rounded-[32px] bg-app-white h-[400px] max-w-[900px] w-full">
      <div
        className=" flex absolute p-8  w-full justify-end text-app-black cursor-pointer"
        onClick={() => {
          Swal.close();
        }}
      >
        <IoMdClose />
      </div>
      <div className="max-w-[417px] max-h-[400px] bg-[url('/img/modal/outScreen.png')] w-auto h-auto bg-cover pl-16 pr-8 flex flex-col justify-center gap-8">
        <Image
          src={'/svg/pentaDollar.svg'}
          width={42}
          height={48}
          alt=""
          className="pt-10"
        />
        <p className="app-title-3 font-semibold text-app-white text-left">
          Ahora, veamos cuál es el salario de los programadores actuamente
        </p>
      </div>
      <div className="mx-auto w-full flex flex-col justify-center gap-4 px-16  mt-3">
        <p className="text-start text-[#303E49] app-title-5 pb-1">
          Compara estos países:
        </p>
        {countries.map((country) => (
          <Link key={country.name} href={country.ytlink} target="_blank">
            <Button
              color="red"
              variant="outlined"
              size="full"
              className="hover:bg-red-300"
            >
              {country.name}
            </Button>
          </Link>
        ))}
      </div>
    </aside>
  );
}
