import { appModal } from '@/lib/services/notifications.services';
import { useEffect } from 'react';

function SweetAlertOnScroll() {
  useEffect(() => {
    function handleScroll() {
      const triggerPosition = window.innerHeight - 100;
      const currentPosition = window.pageYOffset;

      if (currentPosition >= triggerPosition) {
        appModal.fire({
          html: (
            <div className="bg-app-red p-8">
              <p>Esto es un modal que se muestra on scroll</p>
              <button
                className="bg-blue-400 p-2"
                onClick={() => appModal.close()}
              >
                Cerrar
              </button>
            </div>
          ),
          position: 'bottom-right',
          allowOutsideClick: true,
          backdrop: false,
          allowEnterKey: false,
        });
        window.removeEventListener('scroll', handleScroll);
      }
    }

    window.addEventListener('scroll', handleScroll);

    return () => {
      window.removeEventListener('scroll', handleScroll);
    };
  }, []);

  return null;
}

export default SweetAlertOnScroll;
