import Image from 'next/image';
import Link from 'next/link';
import { IoMdClose } from 'react-icons/io';
import Swal from 'sweetalert2';

const CreatedByStudents = () => {
  return (
    <aside className="bg-app-white h-[365px] max-w-sm md:max-w-[900px] w-full rounded-[34px]">
      <div
        className=" flex absolute p-8  w-full justify-end text-white cursor-pointer"
        onClick={() => {
          Swal.close();
        }}
      >
        <IoMdClose />
      </div>
      <div className="max-w-sm md:max-w-[486px] max-h-[365px] bg-[url('/img/modal/bottomScreen.png')] w-auto h-[100%] px-8 md:px-16 flex flex-col justify-center gap-6 rounded-[34px]">
        <Image src={'/svg/schoolVector.svg'} width={42} height={48} alt="" />
        <p className="font-semibold text-app-white text-center md:text-left app-title-3 pb-[10px]">
          ¿Sabías que esta página fue hecha por nuestros egresados?
        </p>
        <Link href={'collaborators'} target="_blank">
          <button className="transition-all hover:brightness-125 hover:scale-105 py-[15px] px-[48px] w-max rounded-[30px] text-[16px] shadow-sm bg-[#EA4959] text-white">
            Ver lista de colaboradores
          </button>
        </Link>
      </div>
    </aside>
  );
};

export default CreatedByStudents;
