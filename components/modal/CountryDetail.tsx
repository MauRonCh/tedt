import Image from 'next/image';
import { useState } from 'react';
import { IoMdClose } from 'react-icons/io';
import Swal from 'sweetalert2';
import dataCountry from '../../lib/data/dataCountry.json';
import moneyIcon from '../../public/img/financing/moneyIcon.png';

interface CountryData {
  id: number;
  country: string;
  titleA: string;
  titleB: string;
  bodyA: string;
  bodyB: string;
  optionA: string;
  optionB: string;
  optionC: string;
  footer: string;
}

const CountryDetail = () => {
  const [countryInfo, setCountryInfo] = useState<CountryData | undefined>(
    dataCountry[0]
  );
  const handleClick = (id: number) => {
    const currentCountry = dataCountry.find((item) => item.id === id);
    setCountryInfo(currentCountry);
  };

  const checkIfIsSelected = (id: number) => {
    return countryInfo?.id === id ? true : false;
  };

  return (
    <div className="flex flex-col relative rounded-[34px]">
      <div
        className="text-black absolute top-5 right-10 cursor-pointer"
        onClick={() => {
          Swal.close();
        }}
      >
        <IoMdClose />
      </div>
      <div className="bg-app-white w-[100%] max-w-[1000px] h-full md:h-[74vh] m-0 flex flex-col  md:flex-row rounded-[30px]">
        <div className=" w-full md:w-1/3  md:bg-ari-per-country-bg bg-cover bg-no-repeat flex flex-col justify-end p-10 ">
          <Image src={moneyIcon} alt="" className="ml-5 mb-2" />
          <h2 className=" font-semibold md:text-white text-[25px] md:mb-8 mt-2">
            Detalle de tu pago compromiso
          </h2>
        </div>
        <div className="w-full md:w-2/3 px-4 md:px-[60px] pb-[60px] md:p-[60px]  flex flex-col justify-between">
          <div className=" flex flex-col gap-9 ">
            <div className=" flex  flex-wrap gap-3 justify-center">
              {dataCountry?.map((country) => {
                return (
                  <button
                    key={country?.id}
                    onClick={() => {
                      handleClick(country.id);
                    }}
                    className={`py-2 px-7 rounded-[30px] text-[14px] shadow-sm truncate ${
                      checkIfIsSelected(country.id)
                        ? 'bg-[#3A78F2] text-white'
                        : 'bg-[#F2F6FF] '
                    }`}
                  >
                    {country.country}
                  </button>
                );
              })}
            </div>

            <div className=" flex flex-col gap-3">
              <h4 className=" text-[20px] text-black font-medium  text-center md:text-left">
                {countryInfo?.titleA || dataCountry[0].titleA}
              </h4>
              <p className=" text-[15px] text-[#7C7C7C] leading-6 sp tracking-wide text-center md:text-left">
                {countryInfo?.bodyA || dataCountry[0].bodyA}
              </p>
              <ul>
                <li className=" text-[15px] text-[#7C7C7C] leading-6 sp tracking-wide text-center md:text-left">
                  {countryInfo?.optionA || dataCountry[0].optionA}
                </li>
                <li className=" text-[15px] text-[#7C7C7C] leading-6 sp tracking-wide text-center md:text-left">
                  {countryInfo?.optionB || dataCountry[0].optionA}
                </li>
                <li className=" text-[15px] text-[#7C7C7C] leading-6 sp tracking-wide text-center md:text-left">
                  {countryInfo?.optionC || dataCountry[0].optionC}
                </li>
              </ul>
              <h4 className=" text-[20px] text-black font-medium text-center md:text-left">
                {countryInfo?.titleB}
              </h4>
              <p className=" text-[15px] text-[#7C7C7C] leading-6 sp tracking-wide text-center md:text-left">
                {countryInfo?.bodyB || dataCountry[0].bodyB}
              </p>
            </div>
          </div>
          <div className=" flex justify-items-end">
            <p className=" text-[#232425] text-[15px] text-center md:text-left pt-3">
              {'* '} {countryInfo?.footer || dataCountry[0].footer}
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CountryDetail;
