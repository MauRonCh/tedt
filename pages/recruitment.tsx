import ButtonIcon from '@/components/assets/ButtonIcon';
import Triangles from '@/components/assets/Triangles';
import Button from '@/components/buttons/Button';
import BrandsSlider from '@/components/sliders/BrandsSlider';
import ReclutingSlider from '@/components/sliders/ReclutingSlider';

export default function Recruitment() {
  return (
    <>
      <section className="app-banner pt-[250px] pb-36 bg-[url(/img/recluting/hero.png)] bg-center bg-no-repeat bg-cover sm:pb-[185px]">
        <Triangles
          className="absolute rotate-[75deg] md:rotate-[60deg] -left-9 md:left-[5vw] bottom-5 md:bottom-28 w-20 h-20 md:w-32 md:h-32 lg:left-[15vw]"
          colorFill="#3A78F2"
          colorOutline="#3A78F2"
        />
        <Triangles
          colorFill=""
          className="absolute top-32 md:top-64 -right-12 md:right-[7vw] -rotate-[70deg] w-24 h-24 md:w-32 md:h-32 lg:right-[15vw]"
        />
        <div className="app-container grid place-items-center text-center">
          <p className="app-title-1 text-app-blue whitespace-nowrap pb-4">
            Recluta talento tech
          </p>
          <p className="app-title-5 text-app-white w-[80%] max-w-[450px]">
            Encuentra a tu próximo ingeniero ofreciendo una pasantía o
            contratando a un recién egresado de Academlo.
          </p>
        </div>
      </section>
      <section className="pt-14 pb-11 sm:pt-24 sm:pb-20">
        <div className="max-w-[990px] mx-auto">
          <p className="app-title-2 text-app-blue mb-9 sm:mb-12">
            &#60;/&#62; POR QUÉ CONTRATAR A NUESTROS EGRESADOS?
          </p>
          <ReclutingSlider />
        </div>
      </section>
      <section className="app-banner bg-app-light pt-10 pb-24 sm:pt-[85px] sm:pb-[70px]">
        <p className="app-title-7 text-app-black text-center mb-16 px-4 sm:mb-28">
          Empresas que confían en nuestros egresados
        </p>
        <div className="mb-12">
          <BrandsSlider />
        </div>
        <BrandsSlider reverse={true} />
        <Button color="red" size="lg" className="hidden mx-auto mt-28 sm:block">
          Solicita asesoramiento personalizado
          <ButtonIcon color="white" />
        </Button>
      </section>
      <section className="app-banner bg-app-black md:flex md:flex-row-reverse">
        <div className="app-container">
          <form className="w-[85%] max-w-[400px] mx-auto text-app-gray pt-20 pb-16 sm:pt-28 sm:pb-24">
            <p className="app-title-7 text-app-white mb-5">¡Contáctanos!</p>
            <p className="mb-6">
              Si quieres saber más de nosostros o tienes dudas puntuales.
            </p>
            <p className="text-sm mb-5 sm:mb-12">
              Dejanos tu mail y te contactaremos
            </p>
            <input
              className="w-full mx-auto block py-2 px-4 rounded-[20px] border-[1px] outline-app-white border-app-white bg-transparent placeholder-app-gray text-[13px] text-app-white mb-2"
              type="text"
              placeholder="Escribe tu mail"
            />
            <textarea
              className="w-full min-h-[100px] mx-auto block p-4 rounded-[20px] border-[1px] outline-app-white border-app-white bg-transparent placeholder-app-gray text-[13px] text-app-white leading-6 mb-6"
              rows={3}
              name=""
              id=""
              placeholder="Ejemplo: Hola, quiero tener más detalles sobre cómo aplicar a la carrera de Full Stack. Saludos!"
            ></textarea>
            <Button color="red" className="mx-auto">
              Enviar
              <ButtonIcon color="white" />
            </Button>
          </form>
        </div>
        <div className="w-full h-[59vw] max-h-[610px] relative bg-[url(/img/recluting/contactbanner.png)] bg-center bg-cover bg-no-repeat">
          <div className="hidden w-[100px] h-full absolute right-0 bg-gradient-to-r from-transparent to-app-black md:block"></div>
        </div>
      </section>
    </>
  );
}
