import ButtonIcon from '@/components/assets/ButtonIcon';
import Hexagons from '@/components/assets/Hexagons';
import Button from '@/components/buttons/Button';
import { GetServerSideProps } from 'next';
import Image from 'next/image';
import { useState } from 'react';
import questions from '../lib/data/questionsData.json';

export const getServerSideProps: GetServerSideProps = async (ctx) => {
  const { q = '0' } = ctx.query;

  return {
    props: {
      q,
    },
  };
};

export default function Faq({ q }: any) {
  const [faq, setFaq] = useState(+q);

  const toggleFaqs = (i: any) => {
    if (faq === i) {
      return setFaq(999);
    }
    return setFaq(i);
  };

  return (
    <div>
      <section className="flex flex-col md:flex-row items-center relative bg-[rgba(235,74,90,0.75)] app-banner h-[482px]">
        <Image
          src="/img/faq/hero.png"
          width={900}
          height={200}
          alt="a"
          className="absolute w-full h-full object-top object-cover -z-10"
          priority
        />
        <h3 className="ml-5 xl:ml-80 mt-52 sm:mt-28 sm:pt-20 sm:ml-[12vw] w-[270px] sm:w-[400px] mr-auto text-2xl sm:text-4xl font-semibold text-app-white">
          No te quedes con ninguna duda.
        </h3>
        <aside className="flex flex-col gap-4 px-12 pt-14 mx-10 md:mr-20 xl:mr-80 bg-app-black max-w-[500px] w-11/12 h-[360px] relative top-10 md:top-32 shadow-[0px_10px_60px_0px_rgba(0,0,0,0.25)]">
          <h3 className="text-app-white app-title-2">¡Contáctanos!</h3>
          <div className="text-app-gray app-title-6 leading-6">
            <p>Si quieres saber más de nosostros o tienes dudas puntuales.</p>
            <p>Dejanos tu mail y te contactaremos</p>
          </div>
          <form className="flex flex-col justify-center gap-6 my-4">
            <input
              type="email"
              placeholder="Escribe tu mail"
              className="w-full border border-app-white bg-transparent py-2 px-4 rounded-3xl placeholder:text-app-gray text-app-white"
            />
            <button
              type="submit"
              className="mx-auto flex items-center justify-center gap-2 rounded-3xl bg-app-red w-32 h-10 text-app-white hover:scale-105 hover:brightness-110 duration-100"
            >
              Enviar
              <ButtonIcon color="white" />
            </button>
          </form>
        </aside>
      </section>
      <section className="pt-44">
        <h3 className="text-center app-title-4 text-app-red">FAQs generales</h3>
        <h1 className="text-center app-title-1 text-app-red">
          Preguntas frecuentes
        </h1>
        <ul className="max-w-5xl mx-auto my-16 md:my-24 shadow-[0px_10px_60px_0px_rgba(226,236,249)]">
          {questions['frequent-questions'].map((question) => (
            <li key={question.id}>
              <h4
                id={'question' + question.id}
                onClick={() => toggleFaqs(question.id)}
                className={`duration-700 ${
                  faq === question.id ? 'text-app-blue' : 'text-app-black'
                } grid grid-cols-[10fr_1fr] justify-items-stretch items-center app-title-4 py-4 md:py-7 px-5 md:px-16 scroll-m-20 cursor-pointer`}
              >
                <span>{question.question}</span>
                <ButtonIcon
                  color={faq === question.id ? 'blue' : 'red'}
                  className={`justify-self-end transition-transform ${
                    faq === question.id ? '-rotate-90' : 'rotate-90'
                  }`}
                />
              </h4>
              <div
                className={`transition-all duration-700 text-app-blue target:max-h-[800px] ${
                  faq === question.id
                    ? 'max-h-[800px] h-auto border-y-[40px] ease-in-out'
                    : 'max-h-0 border-y-8 ease-out'
                } app-title-6 text-app-gray bg-app-light border-transparent px-5 md:px-16 overflow-hidden target:h-auto target:border-y-[40px] target:ease-in-out`}
                dangerouslySetInnerHTML={{ __html: question.answer }}
              ></div>
            </li>
          ))}
        </ul>
      </section>
      {/* DEJA ATRÁS TUS MIEDOS */}
      <section className="app-banner bg-app-light">
        <Hexagons className="absolute right-[2vw] -top-8 w-28 h-36 lg:-top-12 lg:w-40 lg:h-56" />
        <div className="app-container flex-col md:flex-row mx-auto flex justify-center items-center gap-10 md:gap-20 pt-32 pb-28">
          <p className="w-full max-w-[480px] uppercase app-title-2 text-app-blue sm:leading-10 text-center md:text-left ">
            deja atrás tus miedos, las grandes empresas te están necesitando
            ¿estas listo?
          </p>
          <Button className="w-full max-w-[360px] md:min-w-[360px]" color="red">
            <ButtonIcon icon="dino" color="white" />
            <p>¡Sí, quiero dejar de huir!</p>
            <ButtonIcon icon="person" color="white" />
          </Button>
        </div>
      </section>
    </div>
  );
}
