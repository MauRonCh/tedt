import React from 'react';
import collaborators from '../lib/data/collaborators.json';
import Link from 'next/link';

export default function Collaborators() {
  return (
    <div className="app-banner min-h-screen pt-32 flex justify-center items-end bg-[url(/img/collaborators/hero.png)] bg-center bg-cover bg-no-repeat">
      <div className="app-container max-w-[650px]">
        <p className="text-app-red app-title-2 font-normal mb-6 self-start sm:mb-12">
          &#60; Estudiantes colaboradores /&#62;
        </p>
        <div className="w-full min-h-[720px] flex flex-col gap-7 p-7 bg-app-black sm:p-14 ">
          <div className="flex gap-8 text-app-gray">
            <p>0</p>
            <p className="text-app-gray">
              &#60;
              <span className="text-app-blue ml-2">Linkedin : L</span>
              <span className="text-app-white mx-2">;</span>
              /&#62;
            </p>
          </div>
          {collaborators?.map((collaborator, index) => (
            <div key={collaborator.id} className="flex gap-8 text-app-gray">
              <p>{index + 1}</p>
              <Link
                target="_blank"
                className="whitespace-nowrap text-app-blue"
                href={collaborator.linkedin}
              >
                L <span className="text-app-white ml-2">;</span>
              </Link>
              <p className="duration-300 hover:text-app-white">
                {collaborator.name}
              </p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
