import ButtonIcon from '@/components/assets/ButtonIcon';
import Hexagons from '@/components/assets/Hexagons';
import Triangles from '@/components/assets/Triangles';
import Button from '@/components/buttons/Button';
import ProcessCard from '@/components/cards/ProcessCard';
import AcademloPlusSlider from '@/components/sliders/AcademloPlusSlider';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import modules from '../lib/data/modulesInfo.json';
import questions from '../lib/data/questionsData.json';
import startDates from '../lib/data/start-dates/dates-Career.json';

export default function Career() {
  // useAlertOnScroll(4500, <CreatedByStudents />, {
  //   position: 'bottom-right',
  //   allowOutsideClick: false,
  //   backdrop: false,
  // });

  const listCareerHero = [
    'Desarrolla proyectos para tu portafolio',
    'Recibe asesorías personalizadas',
    'Forma parte de una gran red de apoyo',
  ];

  const [selected, setSelected] = useState(0);
  const [faq, setFaq] = useState(null);

  const toggle = (i: number) => {
    setSelected(i);
  };

  const toggleFaqs = (i: any) => {
    if (faq === i) {
      return setFaq(null);
    }

    setFaq(i);
  };

  return (
    <div className="[&>*]:lg:px-52">
      {/* PORTADA */}
      <section className="app-banner text-app-white h-[580px] bg-gradient-to-t from-[#232425] to-[rgba(35,36,37,0)]">
        <div className="absolute w-full h-full bg-app-black top-0 left-0 -z-10"></div>
        <Image
          src={'/img/career/hero.png'}
          alt="hero"
          width={1000}
          height={500}
          className="app-banner absolute -z-10 w-screen h-[580px] object-cover"
          placeholder="blur"
          blurDataURL={'/img/career/hero.png'}
        />
        <div className="app-container pt-40 flex flex-col gap-4">
          <h3 className="flex gap-2 app-title-5">
            <div className="relative pt-[3px]">
              <div className="absolute w-4 h-4 rounded-full bg-app-red animate-ping"></div>
              <div className="w-4 h-4 rounded-full bg-app-red animate-pulse"></div>
            </div>
            Online en vivo
          </h3>
          <h1 className="app-title-7 max-w-[470px]">
            Curso de Desarrollo Web Full-Stack y Ciencias de la Computación
          </h1>
          <ul className="app-title-6 flex flex-col gap-2 pb-[72px]">
            {listCareerHero.map((list) => (
              <li key={list} className="flex gap-4 items-center">
                <Image
                  src={'/svg/check.svg'}
                  width={20}
                  height={20}
                  alt="check"
                />
                {list}
              </li>
            ))}
          </ul>
          <Link
            href="https://aplicacion.academlo.com/signup/"
            className="block relative z-20"
            target="_blank"
          >
            <Button color="white">
              Aplica ahora
              <ButtonIcon color="red" />
            </Button>
          </Link>
        </div>
      </section>
      {/* PROCESO ACADEMLO */}
      <section className="app-banner bg-app-black">
        <div className="app-container py-[72px] md:py-24">
          <h2 className="app-title-2 text-app-red w-full mb-5 md:mb-12">
            &#60;/&#62; CUÁL ES EL PROCESO ACADEMLO?
          </h2>
          <ProcessCard alternate />
        </div>
      </section>

      {/* MÓDULOS DEL PROGRAMA */}
      <section
        id="modules"
        className="app-banner bg-app-light pt-24 pb-20 md:pb-36"
      >
        <div className="app-container px-0.5 sm:px-4 gap-9 sm:gap-14 grid grid-rows-[0.2fr_1fr_0.5fr_1fr_0.5fr)] md:grid-rows-[0.2fr_1fr] md:grid-cols-2">
          <h2 className="app-title-2 text-app-red md:col-span-2">
            &#60;/&#62; MÓDULOS DEL PROGRAMA
          </h2>
          {/* SECCIÓN 1 */}
          <ul className="m-auto min-h-full">
            {modules.modules.map((module, i) => (
              <li className="pl-4 max-w-[518px] relative" key={i}>
                <div onClick={() => toggle(i)} className="cursor-pointer">
                  {/* TÍTULO CLASE Y CÍRCULO */}
                  <h3 className="app-title-4 text-app-blue items-center gap-4 inline-flex">
                    <span className="w-3 h-3 bg-app-blue rounded-full border-app-blue p-1 border-2" />
                    <span
                      className={`w-5 h-5 rounded-full border-app-blue p-1 border absolute left-3 transition-opacity duration-500 ${
                        selected == i ? 'opacity-100' : 'opacity-0'
                      }`}
                    />
                    {module.title}
                  </h3>
                </div>
                <div className="flex">
                  {/* EFECTO LÍNEA */}
                  <span
                    className={`left-5 top-3 w-1 duration-700 ${
                      selected == i
                        ? 'max-h-[500px] h-full sm:h-[105%] ease-[cubic-bezier(1,0,1,0)]'
                        : 'max-h-[100px] h-[120%] lg:h-full ease-[cubic-bezier(0,1,0,1)]'
                    } ${
                      i == modules.modules.length - 1
                        ? 'bg-transparent'
                        : 'bg-app-blue'
                    } absolute`}
                  />
                  {/* TEXTO */}
                  <p
                    className={`my-2 md:my-4 app-title-6 pl-10 overflow-hidden duration-700 text-app-gray ${
                      selected == i
                        ? 'max-h-[500px] h-auto ease-[cubic-bezier(1,0,1,0)]'
                        : 'max-h-0 ease-[cubic-bezier(0,1,0,1)]'
                    }`}
                  >
                    {module.description}
                  </p>
                </div>
              </li>
            ))}
          </ul>
          <Image
            src={'/img/career/modulesImg.png'}
            width={400}
            height={350}
            alt="a"
            className="w-full h-full max-w-[380px] max-h-[340px] object-contain m-auto"
          />
          {/* SECCIÓN 2 */}
          <ul className="m-auto max-w-[518px] md:row-start-3 md:col-start-2">
            {modules.requeriments.map((req, i) => (
              <li key={req.id} className="relative">
                <span
                  className={`left-1 top-3 w-1 max-h-[500px] h-[115%] absolute ${
                    i === modules.requeriments.length - 1
                      ? 'bg-transparent'
                      : 'bg-app-blue'
                  }`}
                />
                <h3 className="app-title-4 text-app-blue flex items-center gap-2 md:gap-4">
                  <span className="w-3 h-3 bg-app-blue rounded-full border-app-blue p-1 border-2" />
                  {req.title}
                </h3>
                <ul className="my-5 pl-2 md:pl-8">
                  {req['program-study'].map((list) => (
                    <li
                      className="list-disc app-title-6 ml-10 text-app-gray"
                      key={list}
                    >
                      {list}
                    </li>
                  ))}
                </ul>
              </li>
            ))}
            <Button color="white" size="xl" className="mx-auto mt-7">
              Ver ejemplo de la prueba <ButtonIcon color="red" />
            </Button>
          </ul>
          <Image
            src={'/img/career/modulesImg.png'}
            width={400}
            height={350}
            alt="a"
            className="w-full h-full max-w-[380px] max-h-[340px] object-contain m-auto"
          />
        </div>
      </section>
      {/* ACTUALIZARME */}
      <section className="relative app-banner flex gap-12">
        <Triangles className="absolute -rotate-[99deg] -top-10 left-[5vw] w-20 h-20" />
        <div className="app-container md:px-0 py-28 flex justify-center items-center flex-col md:flex-row gap-[60px]">
          <p className="max-w-lg app-title-2 text-app-blue uppercase text-center md:text-left">
            Las grandes empresas están buscando empleados con las habilidades
            que te brinda éste curso. ¡Actualízate ya!
          </p>
          <Link href="https://aplicacion.academlo.com/signup/" target="_blank">
            <Button color="red" size="lg">
              <ButtonIcon icon="dino" color="white" />
              <p>¡Quiero actualizarme!</p>
              <ButtonIcon icon="person" color="white" />
            </Button>
          </Link>
        </div>
      </section>

      {/* FECHAS DE INICIO */}
      <section className="app-banner">
        <div className="app-container pb-24 md:py-24">
          <p className="text-center app-title-4 pb-2 sm:pb-8">
            Próximas fechas de inicio:
          </p>
          <div className="mx-auto w-full max-w-5xl py-16 border-app-red border-2 rounded-[20px]">
            <ul className="flex flex-col gap-10 md:gap-20">
              {startDates.map((date) => (
                <li
                  key={date.id}
                  className="flex justify-evenly items-center flex-row gap-2 md:gap-0"
                >
                  <div className="flex flex-col gap-2 md:flex-row md:gap-20">
                    <p className="app-title-4">{'Inicia ' + date.date}</p>
                    <p className="app-title-4 text-app-red">
                      {date.vacantsText}
                    </p>
                  </div>
                  <Link href={date.link} target="_blank">
                    <Button color="red">
                      Aplicar ahora <ButtonIcon color="white" />
                    </Button>
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </section>

      <section className="app-banner bg-app-light">
        <Hexagons className="absolute right-10 -top-20 w-28 h-36 lg:-top-28 lg:w-40 lg:h-56" />
        <div className="app-container py-20 grid grid-rows-1 md:grid-cols-[1.5fr_2fr] sm:grid-rows-2 gap-5">
          <div className="flex flex-col gap-4">
            <h4 className="text-app-red app-title-4">
              Siguiente etapa en tu carrera
            </h4>
            <h2 className="text-app-red app-title-1">Academlo +</h2>
          </div>
          <article>
            Después de terminar todos los módulos de la carrera podrás seguir
            aprendiendo en Academlo plus, un programa de educación continua con
            clases grabadas que actualizamos constantemente para que siempre
            estés a la vanguardia.
          </article>
          <AcademloPlusSlider />

          <Link
            href="https://plus.academlo.com"
            className="md:col-span-2"
            target="_blank"
          >
            <Button size="lg" color="red" className="m-auto flex gap-2">
              Ir a Academlo Plus
              <ButtonIcon color="white" />
            </Button>
          </Link>
        </div>
      </section>
      <section className="py-16 md:py-32 px-0">
        <h4 className="app-title-4 text-app-red text-center">
          FAQs sobre la carrera
        </h4>
        <h2 className="app-title-1 text-app-red text-center">
          Preguntas frecuentes
        </h2>

        <ul className="mt-16 md:mt-20 shadow-[0px_10px_60px_0px_rgba(226,236,249)] ">
          {questions.careerPage.map((question, i) => (
            <li key={question.id}>
              <h3
                onClick={() => toggleFaqs(i)}
                className="cursor-pointer pl-8 md:pl-16 pr-8 py-6 app-title-4 grid grid-cols-[1.5fr_0.1fr] items-center justify-between"
              >
                {question.question}
                <ButtonIcon
                  color="red"
                  className={`transition-transform justify-self-end ${
                    faq === i ? '-rotate-90' : 'rotate-90'
                  }`}
                />
              </h3>
              <p
                className={`duration-700 transition-all overflow-hidden app-title-6 bg-app-light px-8 md:px-16 ${
                  faq === i ? 'max-h-[300px] h-auto py-8' : 'max-h-0 pt-4 pb-0'
                }`}
              >
                {question.answer}
              </p>
            </li>
          ))}
        </ul>
      </section>
    </div>
  );
}
