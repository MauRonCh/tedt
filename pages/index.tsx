import RemarkWord from '@/components/animations/RemarkWord';
import ButtonIcon from '@/components/assets/ButtonIcon';
import Hexagons from '@/components/assets/Hexagons';
import Triangles from '@/components/assets/Triangles';
import Button from '@/components/buttons/Button';
import NextStart from '@/components/cards/NextStart';
import ProcessCard from '@/components/cards/ProcessCard';
import CreatedByStudents from '@/components/modal/CreatedByStudents';
import BrandsSlider from '@/components/sliders/BrandsSlider';
import ExperienceSlider from '@/components/sliders/ExperienceSlider';
import StoriesSlider from '@/components/sliders/StoriesSlider';
import useAlertOnScroll from '@/lib/hooks/useAlertOnScroll';
import Link from 'next/link';
import TypingAnimation from '../components/animations/TypingAnimation';

export default function Home() {
  useAlertOnScroll(1600, <CreatedByStudents />, {
    position: 'bottom-right',
    allowOutsideClick: false,
    backdrop: false,
  });

  return (
    <div className="[&>*]:lg:px-52 app-banner">
      {/* PORTADA */}
      <section className="app-banner bg-app-red">
        <div className="app-container flex flex-col items-center justify-center pt-[155px] pb-[100px] md:grid md:grid-cols-2">
          <header className="flex flex-col items-center gap-[15px] text-center md:text-left md:items-start">
            <h1 className="max-w-[545px] app-title-1 text-app-white">
              Trabaja como
              <span className="block">
                <TypingAnimation
                  words={['Programador', 'Front-End', 'Back-End', 'Full-Stack']}
                />
              </span>
              ahora.
            </h1>
            <p className="max-w-[460px] app-title-5 text-app-white">
              Aprende programación dese cero y trabaja en las más grandes
              empresas de tecnología.
            </p>
          </header>
          <div className="min-w-[225px] min-h-[205px] w-[50vw] h-[55vw] max-w-[700px] max-h-[610px] mt-[45px] mb-[10px] md:row-span-2 bg-[url('/img/home/hero.png')] bg-center bg-contain bg-no-repeat"></div>
          <footer className="w-full h-[50px] flex flex-col items-center justify-center gap-[25px] md:justify-start md:translate-y-[-180%] lg:translate-y-[-130px] md:flex-row">
            <Link
              href="https://aplicacion.academlo.com/signup/"
              target="_blank"
            >
              <Button color="white">
                <p>Postularme</p>
                <ButtonIcon icon="arrow" color="red" />
              </Button>
            </Link>
            <Link href="/career#modules">
              <Button color="white" variant="outlined">
                Malla curricular
              </Button>
            </Link>
          </footer>
        </div>
      </section>
      {/* TEXTO Y PROCESO */}
      <section className="app-banner">
        <div className="app-container mb-24 md:mb-36 relative">
          <Triangles
            className="absolute -rotate-[100deg] -top-32 left-[5vw] w-28 h-28 lg:w-1/5 lg:h-1/5"
            colorFill=""
          />
          <div className="text-app-red text-center app-title-3 mx-auto my-16 md:mt-24 md:mb-32 relative md:max-w-[900px]">
            ¿Y lo mejor? No tienes que pagar una matrícula alta para tomar
            nuestros cursos. En lugar de eso pagarás una vez que tengas trabajo
            como <RemarkWord word="programador" />
          </div>
          <h2 className="app-title-2 text-app-blue w-full mb-5 md:mb-12">
            &#60;/&#62; ¿CUÁL ES EL PROCESO ACADEMLO?
          </h2>
          <ProcessCard alternate={false} />
          <Link href="/faq">
            <Button color="red" size="lg" className="mx-auto">
              Solicita asesoramiento personalizado
              <ButtonIcon icon="arrow" color="white" />
            </Button>
          </Link>
        </div>
      </section>
      {/* TESTIMONIO DE ESTUDIANTES */}
      <section className="bg-app-blue app-banner flex flex-col items-center pt-[35px] md:pt-[55px]">
        <div className=" app-container relative flex flex-col gap-[15px] text-white md:gap-[30px]">
          <Triangles
            colorOutline="transparent"
            className="absolute -top-20 sm:-top-24 md:-top-36 -right-8 sm:right-0 rotate-[145deg] w-[20%] h-[50%] md:w-44 md:h-44"
          />
          <p className="app-title-1">Testimonios de estudiantes</p>
        </div>
        <div className="bg-app-blue w-screen relative z-10 translate-y-[40px] md:translate-y-[70px]">
          <StoriesSlider viewAll={false} />
          <div className="w-screen h-16 absolute bottom-0 bg-app-white" />
        </div>
      </section>
      {/* MARCAS Y TEXTO */}
      <section className="py-[100px] md:pb-[150px]">
        <div className="app-banner mt-6 sm:mt-20">
          <BrandsSlider />
        </div>
        <div className="app-container">
          <div className="max-w-[650px] mx-auto app-title-3 text-app-gray mt-[55px] text-center md:mt-[75px]">
            Egresados de Academlo trabajan en startups y empresas Top de
            Latinoamérica
            <RemarkWord color="bg-app-cyan" word="y el mundo" />
          </div>
        </div>
      </section>
      {/* EXPERIENCIA ACADEMLO */}
      <section className="app-banner">
        <div className="app-container">
          <h2 className="app-title-2 text-app-blue w-full mb-5 md:mb-12">
            &#60;/&#62; VIVE LA EXPERIENCIA ACADEMLO
          </h2>
          <ExperienceSlider />
        </div>
      </section>
      {/* FECHA DE INICIO */}
      <section className="bg-app-white w-full pt-10 md:pt-20 pb-40 px-4">
        <NextStart />
      </section>
      {/* RECLUTAMIENTO */}
      <section className="app-banner pt-14 pb-9 bg-app-blue sm:py-20">
        <div className="app-container flex flex-col items-center max-w-[1440px] relative text-app-white">
          <Triangles
            colorOutline=""
            className="absolute -top-24 -left-2 md:-left-4 w-20 h-20 md:w-44 md:h-44 -rotate-[150deg]"
          />
          <Triangles
            colorFill=""
            colorOutline="#FFFFFF"
            className="absolute -top-28 md:-top-32 left-4 md:left-8 w-24 h-24 md:w-44 md:h-44 -rotate-[160deg]"
          />
          <p className="text-2xl mb-3 sm:text-[56px] sm:mb-10">
            Recluta talento tech
          </p>
          <p className="text-sm  mb-11 text-center w-[75%] max-w-[660px] sm:text-xl">
            Encuentra a tu próximo ingeniero ofreciendo una pasantía o
            contratando a un recién egresado de Academlo.
          </p>
          <ul className="max-w-[990px] border-[1px] border-app-white rounded-[20px] app-title-4 p-10 mb-5 list-disc">
            <li>
              Contratación sin comisión. Nuestros estudiantes son primero.
            </li>
            <li>Ingenieros de software que no solo saben escribir código</li>
            <li>Buenos comunicadores y colaboradores</li>
            <li>
              Contratación sin comisión. Nuestros estudiantes son primero.
            </li>
          </ul>
          <Link className="underline" href={'/recruitment'}>
            Conoce más del reclutamiento{' '}
            <ButtonIcon
              className="inline-block ml-[2px] mb-[2px]"
              color="white"
            />
          </Link>
        </div>
      </section>
      {/* DEJA ATRÁS TUS MIEDOS */}
      <section className="app-banner bg-app-white">
        <Hexagons className="absolute -right-5 lg:right-2 -top-8 w-28 h-36 lg:-top-12 lg:w-40 lg:h-56" />
        <div className="app-container flex-col md:flex-row mx-auto flex justify-center items-center gap-10 md:gap-20 pt-32 pb-28">
          <p className="w-full max-w-[480px] uppercase app-title-2 text-app-blue leading-[40px] text-center md:text-left ">
            deja atrás tus miedos, las grandes empresas te están necesitando
            ¿estas listo?
          </p>
          <Link href="https://aplicacion.academlo.com/signup/" target="_blank">
            <Button
              className="w-full max-w-[360px] md:min-w-[360px] px-4 md:px-2"
              color="red"
            >
              <ButtonIcon icon="dino" color="white" />
              <p>¡Sí, quiero dejar de huir!</p>
              <ButtonIcon icon="person" color="white" />
            </Button>
          </Link>
        </div>
      </section>
      {/* <SweetAlertOnScroll /> */}
    </div>
  );
}
