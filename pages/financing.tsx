import RemarkWord from '@/components/animations/RemarkWord';
import ButtonIcon from '@/components/assets/ButtonIcon';
import Triangles from '@/components/assets/Triangles';
import Button from '@/components/buttons/Button';
import CountryDetail from '@/components/modal/CountryDetail';
import { appModal } from '@/lib/services/notifications.services';
import Lottie from 'lottie-react-web';
import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';
import financingARI from '../lib/data/financingARI.json';
import calendarAnimation from '../public/animations/Calendario.json';
import percentageAnimation from '../public/animations/Porcentaje.json';
import userAnimation from '../public/animations/Usuario.json';

const Financing = () => {
  const [salary, setSalary] = useState(600);
  const [isView, setIsView] = useState(false);

  const animations: any = {
    percentageAnimation: (
      <Lottie
        options={{
          animationData: percentageAnimation,
        }}
      />
    ),
    userAnimation: (
      <Lottie
        options={{
          animationData: userAnimation,
        }}
      />
    ),
    calendarAnimation: (
      <Lottie
        options={{
          animationData: calendarAnimation,
        }}
      />
    ),
  };

  const changeSalary = (e: any) => {
    let customSalary = e.target.value;
    if (customSalary < 0) {
      setSalary(0);
    } else {
      if (customSalary > 9999) {
        setSalary(9999);
      } else {
        setSalary(customSalary);
      }
    }
  };

  const showDetails = () => {
    appModal.fire({
      html: <CountryDetail />,
      width: 1000,
      padding: 0,
      customClass: {
        htmlContainer: 'app-html-container',
        popup: 'app-popup',
      },
      showCloseButton: false,
    });
  };

  return (
    <div>
      <section className="app-banner relative pt-[250px] pb-[120px] bg-[url(/img/financing/hero.png)] bg-center bg-no-repeat bg-cover">
        <span className="w-full h-full absolute top-0 left-0 bg-app-red opacity-[80%]"></span>
        <div className="app-container relative flex flex-col items-center text-center text-app-white">
          <p className="app-title-1 md:mb-4">¿Qué es el ARI?</p>
          <p className="app-title-5 w-[80%]">
            Solo cuando consigues un trabajo pagas.
          </p>
          <a href="#calculator">
            <Button className="mt-10 w-[190px]" color="white">
              Calcular mi pago
              <ButtonIcon color="red" />
            </Button>
          </a>
        </div>
      </section>
      <section className="app-container relative pt-16 pb-20 md:py-24">
        <p className="max-w-[720px] mx-auto text-center app-title-3 text-app-gray">
          Estudia primero, paga despues. No tienes que pagar una matrícula alta.
          En lugar de eso pagarás una vez que tengas un trabajo como
          <RemarkWord color="bg-app-blue" word="programador" />
        </p>
      </section>
      <section className="app-banner bg-[#EDEDED] pt-10 pb-20 md:pt-20 md:pb-24">
        <div className="app-container relative">
          <Triangles className="w-[75px] h-[75px] sm:w-[150px] sm:h-[150px] absolute top-[-11%] right-2 sm:top-[-12vw] md:top-[-33%]" />
          <div className="text-app-red mb-12 max-w-[1030px] mx-auto">
            <p className="app-title-4 mb-3">¿Qué es el ARI?</p>
            <p className="app-title-1">Acuerdo de Reparto de Ingresos</p>
          </div>
          <div className="w-full flex flex-col justify-center items-center md:flex-row-reverse md:gap-20">
            <div className="w-[330px] h-[190px] relative max-w-[550px] max-h-[310px] bg-app-white shadow-[0px_10px_60px_rgba(0,0,0,0.25)] sm:w-[88vw] sm:h-[50vw]">
              <Image
                onClick={() => setIsView(true)}
                className={`duration-500 cursor-pointer ${
                  isView ? 'opacity-0 z-0' : 'opacity-100 z-20'
                }`}
                src={'/img/financing/previewARI.png'}
                fill={true}
                alt=""
              />
              <iframe
                className={`relative ${
                  isView ? 'opacity-100' : 'opacity-0 z-0'
                }`}
                width="100%"
                height="100%"
                src={`https://www.youtube.com/embed/q9NFKvJQQF8?autoplay=${
                  isView ? '1' : '0'
                }&loop=1&playlist=q9NFKvJQQF8&modestbranding=1&rel=0`}
                allowFullScreen={true}
                title="Academlo ARI"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              ></iframe>
            </div>
            <div className="mt-14 md:mt-0">
              <p className="text-app-blue app-title-4 mb-3 w-[300px]">
                Academlo se paga solo una vez que trabajas
              </p>
              <p className="app-title-6 text-app-gray md:w-[400px]">
                En Academlo diseñamos un Acuerdo de Reparto de Ingresos, un
                modelo innovador de financiamiento que nos permite invertir en
                tu educación. Accede a nuestra carrera sin costo inicial.
              </p>
              <Link href="https://www.youtube.com/@Academlo" target="_blank">
                <Button className="w-full mt-14 md:min-w-[300px]" color="red">
                  Ver más en Youtube
                  <ButtonIcon color="white" />
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </section>
      <section className="pt-16 pb-20 md:py-28">
        <div className="app-container flex flex-col justify-center items-center gap-12 md:items-start md:flex-row md:gap-6 lg:gap-24">
          {financingARI?.map((condition) => (
            <div
              key={condition.id}
              className="flex justify-center items-center md:flex-col"
            >
              <div className="min-w-[150px] lg:max-w-[250px]">
                {animations[condition.lottie]}
              </div>
              <div className="min-w-[200px] md:w-[270px]">
                <p className="app-title-4 text-app-red mb-2">
                  {condition.title}
                </p>
                <p className="app-title-6 text-app-gray">
                  {condition.description}
                </p>
              </div>
            </div>
          ))}
        </div>
      </section>
      <section
        id="calculator"
        className="app-banner bg-[#EDEDED] pt-10 pb-14 scroll-mt-16"
      >
        <div className="app-container md:max-w-[1030px]">
          <div className="text-app-red mb-12">
            <p className="app-title-4 mb-3">¿Cuánto debo pagar?</p>
            <p className="app-title-1">Calcula tus ingresos</p>
          </div>
          <div className="grid gap-14 md:grid-cols-2">
            <div>
              <p className="app-title-4 text-app-blue mb-2 md:w-[300px]">
                Academlo se paga solo una vez que trabajas
              </p>
              <p className="app-title-6 text-app-gray md:w-[400px]">
                No pagues una matricula alta para tomar nuestros cursos, en
                lugar de eso paga el 12% de tu salario una vez que hayas
                conseguido un trabajo como programador.
              </p>
            </div>
            <form className="flex flex-col gap-3 app-title-4 text-app-gray md:gap-8 lg:translate-x-[10%]">
              <div className="flex justify-between">
                <label>Tu salario mensual</label>
                <div className="flex items-center bg-app-white rounded-full w-[50%] h-[30px] text-app-black px-4 md:w-[225px]">
                  <input
                    className="outline-none pr-4 bg-transparent w-[50%] text-right ml-5"
                    onChange={changeSalary}
                    value={salary}
                    type="number"
                  />
                  <p className="w-[50%]">USD</p>
                </div>
              </div>
              <div className="flex justify-between">
                <label>Pago del ARI</label>
                <div className="flex items-center gap-0 bg-app-white rounded-full w-[50%] h-[30px] text-app-black px-4 md:w-[225px]">
                  <input
                    className="outline-none mr-2 bg-transparent w-[50%] text-right"
                    value={(salary * (12 / 100)).toFixed()}
                    type="text"
                    readOnly={true}
                  />
                  <p className="w-[50%] pl-[6.5px]">USD</p>
                </div>
              </div>
              <div className="flex justify-between">
                <label>Tu ingreso al mes</label>
                <div className="flex items-center gap-3 bg-app-white rounded-full w-[50%] h-[30px] text-app-black px-4 md:w-[225px]">
                  <input
                    className="outline-none ml-1 bg-transparent w-[50%] text-right"
                    value={(salary - salary * (12 / 100)).toFixed()}
                    type="text"
                    readOnly={true}
                  />
                  <p className="w-[50%] px-[2.5px]">USD</p>
                </div>
              </div>
            </form>
            <Button
              className="w-full max-w-[340px] mx-auto md:mx-0 md:min-w-[340px]"
              color="red"
              onClick={showDetails}
            >
              Ver detalle por país
              <ButtonIcon color="white" />
            </Button>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Financing;
