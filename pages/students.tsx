import RemarkWord from '@/components/animations/RemarkWord';
import ButtonIcon from '@/components/assets/ButtonIcon';
import Triangles from '@/components/assets/Triangles';
import Button from '@/components/buttons/Button';
import BrandsSlider from '@/components/sliders/BrandsSlider';
import Lottie from 'lottie-react-web';
import Link from 'next/link';
import { useState } from 'react';
import LiteYoutubeEmbed from 'react-lite-youtube-embed';
import financingARI from '../lib/data/financingARI.json';
import stories from '../lib/data/sliders/stories.json';
import calendargeAnimation from '../public/animations/Calendario.json';
import percentageAnimation from '../public/animations/Porcentaje.json';
import userAnimation from '../public/animations/Usuario.json';

const Students = () => {
  const animations: any = {
    percentageAnimation: (
      <Lottie
        options={{
          animationData: percentageAnimation,
        }}
      />
    ),
    userAnimation: (
      <Lottie
        options={{
          animationData: userAnimation,
        }}
      />
    ),
    calendarAnimation: (
      <Lottie
        options={{
          animationData: calendargeAnimation,
        }}
      />
    ),
  };

  const [viewAll, setViewAll] = useState(false);
  return (
    <div className="app-container">
      <section className="app-banner relative pt-64 pb-16 mb-20 bg-[url(/img/students/hero.png)] bg-top bg-no-repeat bg-cover md:pb-32 md:mb-28">
        <span className="w-full h-full bg-[#3A78F2] opacity-[75%] absolute left-0 top-0"></span>
        <div className="app-container relative flex flex-col items-center justify-center gap-10 text-white">
          <div className="text-center">
            <p className="app-title-1 mb-4">Comunidad academlo</p>
            <p className="app-title-5 w-[70%] mx-auto">
              El 80 % de nuestros estudiantes esta trabajando
            </p>
          </div>
          <div className="flex flex-wrap items-center justify-center gap-5 md:gap-8">
            <Link
              href="https://aplicacion.academlo.com/signup/"
              target="_blank"
            >
              <Button color="white">
                <p>Postularme</p>
                <ButtonIcon color="red" />
              </Button>
            </Link>
            <Link href="/career">
              <Button color="white" variant="outlined">
                Malla curricular
              </Button>
            </Link>
          </div>
        </div>
      </section>
      <div className="app-banner">
        <div className="pb-20 md:pb-28">
          <BrandsSlider />
        </div>
        <BrandsSlider reverse={true} />
      </div>
      <section className="app-container py-20 md:py-36">
        <p className="max-w-[710px] mx-auto app-title-3 text-app-gray text-center">
          Egresados de Academlo trabajan en startups y empresas Top de
          Latinoamérica
          <RemarkWord word="y el mundo" color="bg-app-cyan" />
        </p>
      </section>
      <section className="app-banner bg-app-light py-11 md:py-20">
        <Triangles className="absolute right-[3vw] -top-6 md:-top-10 w-20 h-20 md:w-36 md:h-36" />
        <div className="app-container max-w-[980px] flex flex-col gap-8 md:gap-12">
          <div className="text-app-red">
            <p className="app-title-4 mb-4">Testimonios de estudiantes</p>
            <p className="app-title-1">Story student</p>
          </div>
          <div className="flex flex-col gap-8 md:gap-12">
            {viewAll
              ? stories
                  ?.map((story) => (
                    <article
                      key={story.id}
                      className="flex flex-col items-center gap-5 md:flex-row md:gap-16"
                    >
                      <div className="relative w-[83vw] h-[45vw] max-w-[485px] max-h-[260px] md:min-w-[485px]">
                        <LiteYoutubeEmbed id={story.link} title={story.title} />
                        {/* <iframe
                          width="100%"
                          height="100%"
                          src={story.link}
                          title="YouTube video player"
                          loading="lazy"
                        ></iframe> */}
                        {/* <Image
                          className="absolute z-10 left-[50%] top-[50%] translate-x-[-50%] translate-y-[-50%] hover:scale-110 duration-300 cursor-pointer"
                          src={'/svg/playVideo.svg'}
                          width={60}
                          height={60}
                          alt=""
                        /> */}
                      </div>
                      <div>
                        <p className="text-app-blue app-title-4 mb-3 md:max-w-[430px]">
                          {story.title}
                        </p>
                        <p className="text-app-gray app-title-6">
                          {story.author}
                        </p>
                      </div>
                    </article>
                  ))
                  .slice(0, 6)
              : stories
                  ?.map((story) => (
                    <article
                      key={story.id}
                      className="flex flex-col items-center gap-5 md:flex-row md:gap-16"
                    >
                      <div className="relative w-[83vw] h-[45vw] max-w-[485px] max-h-[260px] md:min-w-[485px]">
                        <LiteYoutubeEmbed id={story.link} title={story.title} />
                        {/* <iframe
                          width="100%"
                          height="100%"
                          src={story.link}
                          title="YouTube video player"
                          loading="lazy"
                        ></iframe> */}
                        {/* <Image
                          className="absolute z-10 left-[50%] top-[50%] translate-x-[-50%] translate-y-[-50%] hover:scale-110 duration-300 cursor-pointer"
                          src={'/svg/playVideo.svg'}
                          width={60}
                          height={60}
                          alt=""
                        /> */}
                      </div>
                      <div>
                        <p className="text-app-blue app-title-4 mb-3 md:max-w-[430px]">
                          {story.title}
                        </p>
                        <p className="text-app-gray app-title-6">
                          {story.author}
                        </p>
                      </div>
                    </article>
                  ))
                  .slice(0, 3)}
          </div>
          <Button
            onClick={() => {
              !viewAll
                ? setViewAll(true)
                : window.open(
                    'https://www.youtube.com/playlist?list=PLGPbxI0gm1arqxMhYo6P7KMC6k_3RcDQR'
                  );
            }}
            className="w-full max-w-[340px] mx-auto md:mt-10"
            color="red"
            size="xl"
          >
            <p>{!viewAll ? 'Ver más' : 'Ver más en Youtube'}</p>
            <ButtonIcon
              color="white"
              className={`${!viewAll && 'rotate-90'}`}
            />
          </Button>
        </div>
      </section>
      <section className="pt-16 pb-20 md:pt-[140px] md:pb-[270px] ">
        <div className="app-container flex flex-col justify-center items-center gap-12 md:items-start md:flex-row md:gap-6 lg:gap-24">
          {financingARI?.map((condition) => (
            <div
              key={condition.id}
              className="flex justify-center items-center md:flex-col"
            >
              <div className="min-w-[150px] lg:max-w-[250px]">
                {animations[condition.lottie]}
              </div>
              <div className="min-w-[200px] md:w-[270px]">
                <p className="app-title-4 text-app-red mb-2">
                  {condition.title}
                </p>
                <p className="app-title-6 text-app-gray">
                  {condition.description}
                </p>
              </div>
            </div>
          ))}
        </div>
      </section>
    </div>
  );
};

export default Students;
