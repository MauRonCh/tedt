import ErrorMessages from '@/components/cards/ErrorMessages';

export default function Error() {
  const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  const elements = ['0', '1', '2'];

  return (
    <div className="flex justify-center items-center app-banner h-screen bg-[url('/img/errorImg.png')] w-screen overflow-y-hidden">
      <section className="flex max-w-[820px] w-full h-full max-h-[570px] bg-app-black">
        <ul className="w-10 px-14 py-12 flex flex-col items-center gap-6">
          {numbers.map((num) => (
            <li className={`text-app-gray app-title-5`} key={num}>
              {num}
            </li>
          ))}
        </ul>
        <div className="py-12 flex flex-col">
          <code className="text-app-blue app-title-5">
            &#60; error 404 /&#62;
          </code>
          <h2 className="app-title-3 text-app-red py-5 pl-14">
            Sigues en Academlo peeero...
          </h2>
          <div className="app-title-6 text-app-gray flex flex-col gap-7 justify-center pl-14 pb-6">
            <p>... la página que buscas no ha sido encontrada y si quieres </p>
            <p>puedes quedarte un rato por aquí y leer chistes de nerds</p>
            <p>para programadores:</p>
            <p>ㅤㅤㅤㅤ</p>
          </div>
          <code className="text-app-blue app-title-5">
            &#60; zona nerd /&#62;
            <ErrorMessages />
          </code>
        </div>
      </section>
    </div>
  );
}
